# make this node a member of a common ipsec network
#
# the name of this resource is a tag for a network where nodes in the
# same "network" will have ipsec set up between them.
#
# This is sufficient to setup a tunnel between a cluster of machines,
# but requires a reboot, see the parent ipsec class.
#
# WARNING: default ipsec configuration tunnels only the IP address
# given, which means that this default configuration only tunnels
# IPv4, not IPv6.
#
# Use $peer_networks = $base::public_addresses to tunnel both addresses.
#
# @param peer_ipaddress     the ipsec endpoint address of this ipsec node
# @param peer_networks      a list of networks behind or at this ipsec node
define ipsec::network (
  Stdlib::IP::Address $peer_ipaddress       = $base::public_address,
  Array[Stdlib::IP::Address] $peer_networks = [],
) {
  include ipsec

  $ipsec_conf_file = "/etc/ipsec.conf.d/10-puppet-${name}.conf"
  $ipsec_secrets_file = "/etc/ipsec.secrets.d/10-puppet-${name}.secrets"
  $stored_conftag = "ipsec::peer::${name}"

  $real_peer_networks = Array($peer_networks, true).map |$a| {
    if    $a =~ Stdlib::IP::Address::V4::CIDR     { $a }
    elsif $a =~ Stdlib::IP::Address::V4::Nosubnet { "${a}/32" }
    elsif $a =~ Stdlib::IP::Address::V6::CIDR     { $a }
    elsif $a =~ Stdlib::IP::Address::V6::Nosubnet { "${a}/128" }
    else { fail("Do not know address type for ${a}") }
  }

  @@ipsec::peer{ "${name}-${::hostname}":
    network_name       => $name,
    peer_name          => $::hostname,
    peer_ipaddress     => $peer_ipaddress,
    peer_networks      => $real_peer_networks,
    ipsec_conf_file    => $ipsec_conf_file,
    ipsec_secrets_file => $ipsec_secrets_file,
    tag                => $stored_conftag,
    # those will be overriden on collection, below
    local_name         => undef,
    local_ipaddress    => undef,
  }

  concat { $ipsec_conf_file:
    notify  => Service['ipsec'],
  }
  concat { $ipsec_secrets_file:
    notify => Service['ipsec'],
    mode   => '0400',
  }
  Ipsec::Peer <<| tag == $stored_conftag and peer_name != $::hostname|>> {
    local_name      => $::hostname,
    local_ipaddress => $peer_ipaddress,
    local_networks  => $real_peer_networks,
  }
}
