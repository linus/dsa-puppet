# Set up ntpd, either as time server or client
#
# @param use_local_timeservers  Specify whether to use a list of
#                               time servers rather than the defaults
class ntpsec (
  Boolean $use_local_timeservers = false,
) {
  $localtimeservers = hiera('local-timeservers', [])

  package { 'ntpsec':
    ensure => installed
  }

  service { 'ntpsec':
    ensure  => running,
    require => Package['ntpsec']
  }

  ferm::rule { 'dsa-ntpsec':
    domain      => '(ip ip6)',
    description => 'Allow ntp access',
    rule        => '&SERVICE(udp, 123)'
  }

  if (versioncmp($::lsbmajdistrelease, '11') <= 0) {
    file { '/etc/default/ntpsec':
      source => 'puppet:///modules/ntpsec/etc-default-ntpsec',
      require => Package['ntpsec'],
      notify  => Service['ntpsec']
    }
  }
  file { '/etc/ntpsec/ntp.conf':
    content => template('ntpsec/ntp.conf'),
    notify  => Service['ntpsec'],
    require => Package['ntpsec'],
  }
  file { '/var/lib/ntpsec':
    ensure  => directory,
    owner   => ntpsec,
    group   => ntpsec,
    mode    => '0755',
    require => Package['ntpsec']
  }
  file { '/var/log/ntpsec':
    ensure  => directory,
    owner   => ntpsec,
    group   => ntpsec,
    mode    => '0755',
    require => Package['ntpsec']
  }

  # required by the ntp_states munin plugin
  package { 'libnet-dns-perl':
    ensure => installed
  }
  munin::check { [
      'ntp_offset',
      'ntp_states',
      ]:
  }

  file { '/usr/local/sbin/ntpsec-restart-if-required':
    source => 'puppet:///modules/ntpsec/ntpsec-restart-if-required',
    mode    => '0555',
  }

  if getfromhash($deprecated::nodeinfo, 'timeserver') {
    ferm::rule { 'dsa-nts':
      domain      => '(ip ip6)',
      description => 'Allow nts access',
      rule        => '&SERVICE_RANGE(tcp, 4460, $HOST_DEBIAN)'
    }

    file { "/etc/ntpsec/key.pem":
           content => inline_template('<%= File.read(scope().call_function("hiera", ["paths.auto_certs_dir"]) + "/" + @fqdn + ".key") %>'),
           mode    => '0400',
           owner   => ntpsec,
           group   => ntpsec,
           require => Package['ntpsec'],
           notify  => Service['ntpsec'],
    }

    file { "/etc/ntpsec/cert-chain.pem":
           content => inline_template( @("EOF"),
                                       <%= File.read(scope().call_function("hiera", ["paths.auto_certs_dir"]) + "/" + @fqdn + ".crt") %>
                                       <%= File.read(scope().call_function("hiera", ["paths.auto_certs_dir"]) + "/ca.crt") %>
                                       | EOF
                                     ),
           require => Package['ntpsec'],
           notify  => Service['ntpsec'],
    }
  }

  base::apparmor { '/usr/sbin/ntpd':
    source => 'puppet:///modules/ntpsec/apparmor-local-ntpd',
  }
}
