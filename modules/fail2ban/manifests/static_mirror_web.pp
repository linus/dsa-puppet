# static mirror web server-specific fail2ban setup
#
class fail2ban::static_mirror_web inherits fail2ban {
	file { '/etc/fail2ban/filter.d/dsa-static-mirror-web.conf':
		source  => 'puppet:///modules/fail2ban/filter/dsa-static-mirror-web.conf',
		notify  => Service['fail2ban'],
		require => Package['fail2ban'],
	}
	file { '/etc/fail2ban/jail.d/dsa-static-mirror-web.conf':
		source  => 'puppet:///modules/fail2ban/jail/dsa-static-mirror-web.conf',
		notify  => Service['fail2ban'],
		require => Package['fail2ban'],
	}
}
