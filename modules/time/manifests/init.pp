class time {
	include stdlib
	$localtimeservers = hiera('local-timeservers', [])
	$physicalHost = $deprecated::allnodeinfo[$fqdn]['physicalHost']
	$ntp_use_local_timeservers = hiera('ntp::use_local_timeservers', false)

	if (!$ntp::use_local_timeservers and $dsa_systemd and size($localtimeservers) > 0 and $::is_virtual and $::virtual == 'kvm') {
		include ntp::purge
		include ntpsec::purge
		include systemdtimesyncd
	} else {
		include ntpsec
		include ntp::purge
	}
}
