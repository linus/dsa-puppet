# Convenience class to create CNAME records for DKIM keys
#
# The default parameter values are designed so that in many cases one can simply
#
# dnsextras::dkim::cname { "foo.example.com" : }
#
# @param selector DKIM selector to use for the target key ID. Defaults to "smtpauto"
# @param hostname Hostname to use for the key IDs. Defaults to the local hostname
# @param domain  Domain name to use for the target key ID. Defaults to the local domain
# @param aliasdomain Domain name to use for the aliased key ID. Defaults to $domain
# @param aliasselector DKIM selector to use for the aliased key ID. Defaults to $selector
define dnsextras::dkim::cname (
  $selector = 'smtpauto',
  $hostname = $::hostname,
  $domain = $::domain,
  $aliasdomain = $name,
  $aliasselector = $selector,
) {
  $snippet = "${aliasselector}.${hostname}._domainkey.${aliasdomain}.\tIN\tCNAME\t${selector}.${hostname}._domainkey.${domain}.\n"
  dnsextras::entry{ "dkim-cname-${name}":
                    content => $snippet,
  }
}
