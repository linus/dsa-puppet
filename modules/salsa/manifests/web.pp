#
class salsa::web (
  String $fqdn = $::fqdn,
) inherits salsa {
  include apache2
  include apache2::proxy_http
  include apache2::rewrite
  include apache2::ssl

  $metrics_fqdn = "metrics.${fqdn}"

  ssl::service { [
    'salsa.debian.org',
    'registry.salsa.debian.org',
    'signup.salsa.debian.org',
    'webhook.salsa.debian.org',
    'pages.debian.net',
    $metrics_fqdn,
    ]:
    notify => Exec['service apache2 reload'],
  }

  apache2::site { '010-salsa.debian.org':
    site    => 'salsa.debian.org',
    content => template('salsa/apache-salsa.debian.org.conf.erb'),
  }
  apache2::site { '010-registry.salsa.debian.org': ensure => absent, }
  apache2::site { '011-registry.salsa.debian.org':
    site    => 'registry.salsa.debian.org',
    content => template('salsa/apache-registry.salsa.debian.org.conf.erb'),
  }
  apache2::site { '010-signup.salsa.debian.org': ensure => absent, }
  apache2::site { '011-signup.salsa.debian.org':
    site    => 'signup.salsa.debian.org',
    content => template('salsa/apache-signup.salsa.debian.org.conf.erb'),
  }
  apache2::site { '010-webhook.salsa.debian.org': ensure => absent, }
  apache2::site { '011-webhook.salsa.debian.org':
    site    => 'webhook.salsa.debian.org',
    content => template('salsa/apache-webhook.salsa.debian.org.conf.erb'),
  }
  apache2::site { '010-pages.debian.net': ensure => absent, }
  apache2::site { '011-pages.debian.net':
    site    => 'pages.debian.net',
    content => template('salsa/apache-pages.debian.net.conf.erb'),
  }
  apache2::site { "012-${metrics_fqdn}":
    site    => $metrics_fqdn,
    content => template('salsa/apache-metrics.conf.erb'),
  }
}
