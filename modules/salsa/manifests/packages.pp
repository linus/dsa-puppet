#
class salsa::packages inherits salsa {
  $requirements = [
    'build-essential',
    'bundler',
    'checkinstall',
    'cmake',
    'curl',
    'golang',
    'iotop',
    'libcurl4-openssl-dev',
    'libimage-exiftool-perl',
    'libffi-dev',
    'libgdbm-dev',
    'libicu-dev',
    'libkrb5-dev',
    'libncurses5-dev',
    'libpcre3-dev',
    'libre2-dev',
    'libreadline-dev',
    'libssl-dev',
    'libxml2-dev',
    'libxslt1-dev',
    'libyaml-dev',
    'logrotate',
    'node-mkdirp',
    'node-semver',
    'nodejs',
    'pkg-config',
    'prometheus-node-exporter',
    'python3-docutils',
    'python3-requests',
    'ruby-dev',
    'ruby-sinatra',
    'ruby-sinatra-contrib',
    'ruby-mail',
    'ruby-soap4r',
    'thin',
    'libpq-dev',
    'zlib1g-dev'
  ]

  ensure_packages($requirements, { ensure => 'installed' })

  $mgmt_requirements = [
    'ansible',
    'python3-hkdf',
    'ruby-ldap',
  ]

  ensure_packages($mgmt_requirements, { ensure => 'installed' })

  $registrationapp_requirements = [
    'libapache2-mod-wsgi-py3',
    'libjs-bootstrap',
    'python3',
    'python3-flask',
    'python3-flaskext.wtf',
    'python3-authlib',
  ]

  ensure_packages($registrationapp_requirements, { ensure => 'installed' })
}
