define stunnel4::generic ($client, $verify, $cafile, $accept, $connect, $crlfile=false, $local=false) {

	include stunnel4

	file { "/etc/stunnel/puppet-${name}.conf":
		content => template('stunnel4/stunnel.conf.erb'),
	}

	if $client {
		$certfile = '/etc/ssl/debian/certs/thishost.crt'
		$keyfile = '/etc/ssl/private/thishost.key'
	} else {
		$certfile = '/etc/exim4/ssl/thishost.crt'
		$keyfile = '/etc/exim4/ssl/thishost.key'
	}

	service { "stunnel@puppet-${name}":
		ensure  => running,
		enable  => true,
		require => [
			File["/etc/stunnel/puppet-${name}.conf"],
			File['/etc/tmpfiles.d/stunnel.conf'],
			File['/etc/systemd/system/stunnel@.service'],
			Package['stunnel4'],
		],
		subscribe => [ File[$certfile], File[$keyfile], File["/etc/stunnel/puppet-${name}.conf"] ],
	}
}
