
%w{/srv/build-trees
   /srv/buildd
   /etc/ssh/ssh_host_ed25519_key
   /srv/mirrors/debian
   /srv/mirrors/debian-buildd
   /srv/mirrors/debian-debug
   /srv/mirrors/debian-ports
   /srv/mirrors/debian-security
   /srv/mirrors/debian-security-debug
   /srv/mirrors/public-debian
   /srv/mirrors/public-debian-buildd
   /srv/mirrors/public-debian-debug
   /srv/mirrors/public-debian-ports
   /srv/mirrors/public-debian-security
   /srv/mirrors/public-debian-security-debug
   /lib/udev/rules.d/60-kpartx.rules
   /etc/ssh/ssh_known_hosts
}.each do |path|
	Facter.add("has" + path.gsub(/[\/.-]/,'_')) do
		setcode do
			if FileTest.exist?(path)
				true
			else
				false
			end
		end
	end
end
