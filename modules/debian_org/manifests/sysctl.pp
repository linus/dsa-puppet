# set up default debian.org sysctl things
#
# This includes defaults passed in via hiera, and settings requested via
# the sysctl_request fact.
#
# @param settings
#   Set these sysctl keys to their values.  Individual keys are overriden by
#   the values found in the sysctl_request fact.
class debian_org::sysctl (
  Optional[Hash[Pattern[/\A[a-z0-9_.-]*\z/], Variant[Integer, String]]] $settings,
) {
  file { '/etc/sysctl.d':
    ensure  => directory,
    purge   => true,
    force   => true,
    recurse => true,
  }
  file { '/etc/sysctl.d/99-sysctl.conf':
    ensure => link,
    target => '../sysctl.conf',
    owner  => 'root',
    group  => 'root',
  }
  file { '/etc/sysctl.d/README.sysctl':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0444',
  }
  file { '/etc/sysctl.d/local.conf':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0444',
  }

  $_settings = pick($settings, {}) + pick($facts['sysctl_request'], {})
  $_settings.each |$key, $value| {
    debian_org::sysctl::value { $key:
      value => $value,
    }
  }

  $puppet_sysctl_file = '/etc/sysctl.d/zzz-puppet.conf' # we want that name last, after things like (/usr/lib/sysctl.d/) protect-links.conf)
  concat { $puppet_sysctl_file:
    owner => 'root',
    group => 'root',
    mode  => '0444',
  }
  ~> exec { 'sysctl::reload':
    path        => $facts['path'],
    user        => 'root',
    command     => 'sysctl --system',
    refreshonly => true,
  }
}
