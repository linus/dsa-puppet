# LVM config for the arm servers that make up ganeti1.conova.debian.org
class profile::lvm::ganeti1_conova {
  class { 'dsalvm':
    global_filter  => '[ "a|^/dev/md[0-9]*$|", "r/.*/" ]',
    issue_discards => true,
  }
}
