# buildd configuration
class buildd::buildd {
  file { '/home/buildd/build':
    ensure  => directory,
    mode    => '2750',
    group   => buildd,
    owner   => buildd,
  }
  file { '/home/buildd/logs':
    ensure  => directory,
    mode    => '2750',
    group   => buildd,
    owner   => buildd,
  }
  file { '/home/buildd/old-logs':
    ensure  => directory,
    mode    => '2750',
    group   => buildd,
    owner   => buildd,
  }
  file { '/home/buildd/upload-security':
    ensure  => directory,
    mode    => '2750',
    group   => buildd,
    owner   => buildd,
  }
  file { '/home/buildd/stats':
    ensure  => directory,
    mode    => '2755',
    group   => buildd,
    owner   => buildd,
  }
  file { '/home/buildd/stats/graphs':
    ensure  => directory,
    mode    => '2755',
    group   => buildd,
    owner   => buildd,
  }
  file { '/home/buildd/upload':
    ensure  => directory,
    mode    => '2755',
    group   => buildd,
    owner   => buildd,
  }
  file { '/home/buildd/.forward':
    content  => "|/usr/bin/buildd-mail\n",
    group   => buildd,
    owner   => buildd,
  }

  package { 'buildd':
    ensure => installed,
  }
  file { '/etc/buildd/buildd.conf':
    source  => 'puppet:///modules/buildd/buildd.conf',
    require => Package['buildd'],
  }
  file { '/etc/cron.d/buildd':
    source  => 'puppet:///modules/buildd/cron.d-buildd',
    require => Package['buildd'],
  }

  concat::fragment { 'puppet-crontab--buildd':
    target => '/etc/cron.d/puppet-crontab',
    source  => 'puppet:///modules/buildd/cron.d-dsa-buildd',
    require => Package['debian.org']
  }
  service { 'buildd':
    enable => false,
    ensure => 'stopped',
  }
}
