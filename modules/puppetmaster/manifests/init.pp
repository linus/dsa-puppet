class puppetmaster {
  include apache2

  package { 'puppet-master':
    ensure => installed,
  }
  file { '/etc/puppet/puppetdb.conf':
    source => 'puppet:///modules/puppetmaster/puppetdb.conf'
  }

  ensure_packages(['puppetdb'])
  service { 'puppetdb':
    ensure  => running,
    require => Package['puppetdb'],
  }
  file { '/etc/default/puppetdb':
    source => 'puppet:///modules/puppetmaster/etc-default-puppetdb',
    notify => Service['puppetdb'],
  }

  ferm::rule { 'dsa-puppet':
    description => 'Allow puppet access',
    domain      => '(ip ip6)',
    rule        => '&SERVICE_RANGE(tcp, 8140, $HOST_DEBIAN)',
  }

  file { '/srv/puppet.debian.org/puppet-facts':
    ensure => directory
  }
  concat { '/srv/puppet.debian.org/puppet-facts/onionbalance-services.yaml':
  }
  Concat::Fragment <<| tag == 'onionbalance-services.yaml' |>>
  concat { '/srv/puppet.debian.org/puppet-facts/onionbalancev3-services.yaml':
  }
  Concat::Fragment <<| tag == 'onionbalancev3-services.yaml' |>>

  file { '/etc/cron.d/puppet-update-fastly-ips': ensure => absent, }
  file { '/etc/cron.d/update-fastly-ips': ensure => absent, }
  concat::fragment { 'puppet-crontab---fastly-ips':
    target  => '/etc/cron.d/puppet-crontab',
    content => @(EOF)
      @daily  root  /usr/local/bin/update-fastly-ips /srv/puppet.debian.org/puppet-facts/fastly_ranges.yaml
      | EOF
  }
  file { '/usr/local/bin/update-fastly-ips':
    source => 'puppet:///modules/puppetmaster/update-fastly-ips.sh',
    mode   => '0555',
  }

  file { '/etc/logrotate.d/puppetdb':
    ensure => absent,
  }

  file { '/usr/local/bin/make-tlsa':
    source => 'puppet:///modules/puppetmaster/make_tlsa.py',
    mode   => '0555',
  }

  mon::service { 'check_puppetdb_nodes/puppet - all catalog run':
    on_target => true,
    vars      => {
      apiversion => 4,
      warning    => 720,
      critical   => 1440,
    },
  }

  # This isn't really part of the Puppetmaster setup, but we run it from there for convenience
  mon::service::shell_wrapped { 'SSL certs - LE': # {{{
    check_interval => '60m',
    retry_interval => '15m',
    command        => '/usr/lib/nagios/plugins/dsa-check-cert-expire-dir /srv/puppet.debian.org/from-letsencrypt',
  } # }}}
}
