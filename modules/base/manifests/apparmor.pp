# Install or remove an apparmor local config file
#
# @param source  source of the apparmor config file
# @param content content of the apparmor config file
# @param ensure  present or absent
define base::apparmor (
  Optional[String] $source = undef,
  Optional[String] $content = undef,
  Enum['present','absent'] $ensure = 'present',
) {
  case $ensure {
    present: {
      if ! ($source or $content) {
        fail ( "No configuration found for ${name}" )
      }
    }
    absent:  {}
    default: { fail ( "Unknown ensure value: ${ensure}" ) }
  }

  exec { '/lib/apparmor/apparmor.systemd reload':
    refreshonly => true,
  }

  $temp_name = regsubst($name, '^/', '')
  $bin_name = regsubst($temp_name, '/', '.', 'G')

  $file = "/etc/apparmor.d/local/$bin_name"

  file { $file:
    ensure  => $ensure,
    content => $content,
    source  => $source,
    notify  => Exec['/lib/apparmor/apparmor.systemd reload'],
  }

}
