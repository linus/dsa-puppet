# Hosts with a broken or no RTC (real time clock)
class roles::broken_rtc {
  ensure_packages ( [
    'fake-hwclock',
  ], {
    ensure => 'installed',
  })

  include ntpdate
}
