# = Class: roles::rtc
#
# Setup for machines used by the RTC Team
#
# == Sample Usage:
#
#   include roles::rtc
#
class roles::rtc {

  include profile::prosody

  ssl::service { 'xmpp.debian.org':
    tlsaport => [],
    notify   => Service['prosody'],
  }

  dnsextras::tlsa_record{ 'tlsa-xmpp':
    certfile => '/srv/puppet.debian.org/from-letsencrypt/xmpp.debian.org.crt',
    port     => [5061, 5222, 5269],
    hostname => $::fqdn,
  }

  ferm::rule { 'dsa-xmpp-client-ip4':
    domain      => 'ip',
    description => 'XMPP connections (client to server)',
    rule        => 'proto tcp dport (5222) ACCEPT'
  }
  ferm::rule { 'dsa-xmpp-client-ip6':
    domain      => 'ip6',
    description => 'XMPP connections (client to server)',
    rule        => 'proto tcp dport (5222) ACCEPT'
  }
  ferm::rule { 'dsa-xmpp-server-ip4':
    domain      => 'ip',
    description => 'XMPP connections (server to server)',
    rule        => 'proto tcp dport (5269) ACCEPT'
  }
  ferm::rule { 'dsa-xmpp-server-ip6':
    domain      => 'ip6',
    description => 'XMPP connections (server to server)',
    rule        => 'proto tcp dport (5269) ACCEPT'
  }
  ferm::rule { 'dsa-xmpp-http-ip6':
    domain      => 'ip6',
    description => 'XMPP HTTP Uploads',
    rule        => 'proto tcp dport (5281) ACCEPT'
  }
  ferm::rule { 'dsa-xmpp-http-ip4':
    domain      => 'ip',
    description => 'XMPP HTTP Uploads',
    rule        => 'proto tcp dport (5281) ACCEPT'
  }

  file { '/etc/monit/monit.d/50rtc':
    ensure  => absent,
  }
}
