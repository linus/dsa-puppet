# install packages for building and running container images
class roles::snapshot_container (
) {
  ensure_packages ([
    'mmdebstrap',
    'podman',
    'slirp4netns',
  ])

  debian_org::fact { 'sysctl_request':
    value => { 'kernel.unprivileged_userns_clone' => 1 },
  }

  dsa_systemd::linger { 'snapshot': }

  ensure_packages (['uidmap'])
  file { '/etc/subuid':
    mode    => '0444',
    content => @(EOF),
      snapshot:100000:65536
      | EOF
  }
  file { '/etc/subgid':
    mode    => '0444',
    content => @(EOF),
      snapshot:100000:65536
      | EOF
  }
}
