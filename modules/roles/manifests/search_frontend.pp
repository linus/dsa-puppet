class roles::search_frontend {
  include apache2

  ssl::service { 'search.debian.org':
    notify => Exec['service apache2 reload'],
  }
}
