class roles::www_cgi {
  include apache2
  ssl::service { 'cgi.debian.org': notify  => Exec['service apache2 reload'], }
}
