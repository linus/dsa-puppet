class roles::container_builder {
  $container_builder_01_password = hkdf('/etc/puppet/secret', "docker-registry:image-registry-01.debian.org:${::fqdn}")

  $homedir = '/srv/container-builder.debian.org'
  $username = 'container-builder'

  group { $username:
  }
  user { $username:
    groups     => [$username],
    membership => 'inclusive',
    home       => $homedir,
  }

  file { $homedir:
    ensure => 'directory',
    mode   => '0755',
    owner  => $username,
    group  => $username,
  }

  file { "${homedir}/.docker-registry-credentials":
    mode      => '0400',
    owner     => $username,
    group     => $username,
    # security sensitive, don't show in logs
    show_diff => false,
    content   => @("EOF"),
        container-builder-01:${container_builder_01_password}
        | EOF
  }
}
