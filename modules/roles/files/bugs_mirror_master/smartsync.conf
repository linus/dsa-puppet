---
base: /srv/bugs.debian.org
# Files or directories to monitor.
#
# if you want to monitor a directory recursively, end with a /
# otherwise just elements added/changed/etc directly in that dir
# will be watched.
#
# Note that there is an in-kernel per-user limit of around 8k nodes
# you may watch, and in a recursive dir every single directory counts
# against that limit.  So be careful what to list here.
monitor:
  - spool
  - spool/db-h/
  - spool/user/
  - spool/archive/

  # versions/archive/ - do not monitor, it's huge
  - versions/bin/
  - versions/cl-data/
  - versions/indices/
  # versions/lock/
  - versions/pkg/

  - bugscan
  - bugscan/britney/
  - cgi-bin/
  - etc/
  - perl/
  - pseudopackages/
  - scripts/
  - source/
  - www/

# Files being watched, but which should be ignored anyway
filter:
  - spool/incoming-spamscan
  - spool/debbugs.trace.lock
  - spool/incoming-cleaner
  - spool/debbugs.trace
  - spool/nextnumber.lock
  - source/bak
  - source/tmp
  - bugscan/*
  - '*.new'
  - '*.lock'
  - '.*.swp'
  - '.*.swp?'
  - '*~'

filter_override:
  - bugscan/status
  - bugscan/britney/*

sync:
  incremental:
    - rsync
    #- --progress
    - -a
    - -v
    - -z
    - --timeout=1200
    - --files-from
    - '%%FILENAME%%'
    - '.'
    - debbugs@${HOSTNAME}:/srv/bugs-webmirror.debian.org
  full:
    - rsync
    #- --progress
    - -a
    - -v
    - -z
    - --include-from
    - /home/debbugs-mirror/etc/smartsync.fullsync-list
    - --timeout=3600
    - --delete-after
    - --delete-excluded
    - --filter
    - 'P /.nobackup'
    - '.'
    - debbugs@${HOSTNAME}:/srv/bugs-webmirror.debian.org
  fullsyncflag: '/home/debbugs-mirror/full-sync-flags/${HOSTNAME}'

log:
  filename: '/home/debbugs-mirror/log/sync-${HOSTNAME}.log'
  keep: 15
